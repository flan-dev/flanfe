# FlanFE

A simplistic Misskey client on the web. Hosted [here](https://flan-dev.codeberg.page/flanfe/@main) and [here](https://flanfe.amongtech.cc) currently.

### What's the difference between the two hosting?

The first one is always up to date, as it's made by Codeberg Page and keeps track. The second one is managed by the developer, and includes HSTS as well as compression of the files.

## What does FlanFE aim to be?

### Priority Goals

- [x] Get token and save it to a cookie- or use MiAuth
- [x] A timeline
- [x] timeline actual working refresh button
- [x] Post form (alpha or beta)
- [x] Emojo support
- [ ] React action and view support
- [ ] Quote view support
- [x] Renote action and view support
- [x] Reply view support
- [x] Reply action support
- [x] Profile photo loading
- [X] CW support

### Goals

- [ ] Custom API stuff support
- [ ] Cat mode
- [ ] Profile view support
- [X] Visibility change support
- [x] Visibility icons
- [ ] Profile edit support

## Log on

Generate a single, all-access API token on your instance and save it somewhere. Open Meowkey and it should guide you through the process.



/* index.js
 *
 * Copyright 2022 foreverxml <foreverxml@tuta.io>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

"use strict";
var xhr = new XMLHttpRequest();
var storage = window.localStorage;


/* This function sanitizes text
 * very lightweight, and
 * only removes < and >
 * by converting them to entities
 * in the code.
 *
 * The only argument is the string that is to be sanitized.
*/

function sanitize(text) {
	let sanitized;
	if (text) { sanitized = text.replaceAll(/</g, '&lt;'); }
	return text ? sanitized.replaceAll(/>/g, '&gt;') : null;
}

/* This function displays a prompt similar to window.prompt.
 *
 * Arguments:
 * 1. texts - an object containing text data
 *    texts.prompt is what it asks the user,
 *    texts.ok is that the okay button says,
 *    and texts.cancel is what the cancel button says.
 *
 *    Example:
 *    prompt({"prompt": "Enter your note", "ok": "Send!", "cancel": "Cancel:}, true)
 *
 *    The default value for prompt is "Enter text below:",
 *    for ok it is "OK",
 *    and for cancel it is "Cancel".
 *
 * 2. field - boolean on whether the field should be 3 lines (true) or 1 line (false)
*/

/*
async function prompt(texts, field) {
	return new Promise((resolve, reject) => {
		okay.addEventListener("click", event => {
			resolve(text.value ?? text.innerHTML);
		}, { once: true });
		cancel.addEventListener("click", event => {
			reject(null);
		}, { once: true });
	});
}
*/

/* This function gets the
 * 1. home instance
 * and
 * 2. API token
 * for the logged-in user.
 * The instance is returned first in the array,
 * and the token second.
*/

function getdat() {
	var inst, tok;
	inst = storage.getItem("inst");
	tok = storage.getItem("tok");
	if (inst == undefined || tok == undefined) {
		inst = window.prompt("What is your instance? No https://, just the TLD.");
		tok = window.prompt("What is your full access Misskey token?");
		storage.setItem("inst", inst);
		storage.setItem("tok", tok);
	}
	return [inst, tok];
}

/* This function renders custom emojis
 * into a note.
 *
 * Arguments:
 * 1. text - the string of text that should have the emoji replacement
 * 2. emojis - the standard array of objects with emojis returned by Misskey
*/

function emojify(text, emojis) {
	let len = emojis.length;
	let regex, i;
	try {
		for (i = 0; i < len; i++) {
			regex = new RegExp(`:${emojis[i].name}:`, "g");
			text = text.replaceAll(
				regex,
				`<img src="`+emojis[i].url+`" alt="`+emojis[i].name+`" class="emoji" />`
			);
		}
	} catch (e) {
		console.log(e);
		return text;
	}
	return text;
}
/* This note constructs
 * notes into the timeline of Meowkey.
 * It's the primary function used
 * in Meowkey.
 * It will redirect users to a loaded note
 * if the note is already loaded in the timeline.
 * It also parses followers-only posts differently.
 * See the inside of this function for more documentation.
 *
 * Arguments:
 * 1. note - complete note as fetched from the Misskey API
 *    Please use rendernote(id) if you don't have the note yet.
 *
 * 2. scroll - whether to scroll to the element when it's made.
*/

function innerhtmlmaker(note, scroll) {
	let noteid;
	try {
		noteid = note.id;
	} catch (e) {
		console.log(note.id);
		return;
	}
	if (document.body.contains(document.getElementById(noteid))) {
		if (scroll) {
			document.getElementById(noteid).scrollIntoView();
		}
		return;
	}
	var indat = getdat(); // get instance data
	var catclass = note.user.isCat ? "pfp cat" : "pfp"; // is the user a cat?
	var host, text, usrnm;
	let visibility = note.visibility === "public"
		? "🌐"
		: note.visibility === "home"
		? "🏠"
		: note.visibility === "followers"
		? "🔒"
		: "✉️";
	// render the username
	try {
		host = "@" + note.user.host;
		host = host === "@null" ?
			"@" + note.user.username + " " + visibility:
			"@" + note.user.username + host + " " + visibility;
	} catch (e) {
		console.log(note.id);
		return;
	}
	let name = note.user.name ? emojify(sanitize(note.user.name + " "), note.user.emojis) : note.user.username + " ";

	// render note contents (no renote or reply)
	text = note.cw ?
		"<details><summary>" + emojify(emojify(sanitize(note.cw), note.emojis), note.emojis) + "</summary><p>" + emojify(emojify(sanitize(note.text), note.emojis), note.emojis) + "</p></details>" :
		emojify(emojify(sanitize(note.text), note.emojis), note.emojis);
	let file;
	try {
	if (note.files[0]) {
		let i;
		for (i = 0; i < note.files.length; i++) {
			file = note.files[i];
			if (file.url.includes(".jpg") || file.url.includes(".png") || file.url.includes(".jpe") || file.url.includes(".jpeg") || file.url.includes(".gif") || file.url.includes(".webp")) {
					text += "<img class=\"noteimg\" src=\""+file.url+"\" alt=\""+file.comment+"\" />";
				}
		}
	}
	} catch (e) {
		console.log(e);
	}
	// currently null values are not parsed by this. this will be fixed when file loading is added.

	var origname, origvisib, origpfp, renotetext;
	// is this note a renote? if so...
	if (note.renote) {
		let renotehost, renusernam;
		origname = host;
		origvisib = visibility;
		origname = name;
		// render the username of the poster of the original post
		visibility = note.visibility === "public"
			? "🌐"
			: note.visibility === "home"
			? "🏠"
			: note.visibility === "followers"
			? "🔒"
			: "✉️";
		try {
			host = "@" + note.renote.user.host;
			host = host === "@null" ?
				"@" + note.renote.user.username + " " + visibility :
				"@" + note.renote.user.username + host + " " + visibility;
		} catch (e) {
			console.log(note.id);
			return;
		}
		name = note.renote.user.name ? emojify(sanitize(note.renote.user.name + " "), note.renote.user.emojis) : note.renote.user.username + " ";
		let inlinepfp = `<img class="pfp inline" src="`+note.user.avatarUrl+`" alt="`+sanitize(origname)+`" />`;
		// new noteid in reference for functions
		try {
			noteid = note.renote.id;
		} catch (e) {
			try {
				console.log(note.id);
			} catch (e) {
				console.log("A note threw an error!");
			}
			return;
		}

		// render renote contents
		renotetext = inlinepfp + "<b>" + origname + ` <a href="javascript:void(rendernote('` + note.renote.id + `', true))">renoted</a>:</b>`;
		text = typeof (note.renote.cw) == "string" ?
			`<details><summary>` + emojify(sanitize(note.renote.cw), note.renote.emojis) + `</summary><p>` + emojify(sanitize(note.renote.text), note.renote.emojis) + `</p></details>` :
			emojify(sanitize(note.renote.text), note.renote.emojis);
		// this does not render quotes currently, or replies. those can be rendered separately.
		// however, quotes should be added, open an issue if you want that.
		try {
		if (note.renote.files[0]) {
			let i;
			for (i = 0; i < note.renote.files.length; i++) {
				file = note.renote.files[i];
				if (file.url.includes(".jpg") || file.url.includes(".png") || file.url.includes(".jpe") || file.url.includes(".jpeg") || file.url.includes(".gif") || file.url.includes(".webp")) {
					text += "<img class=\"noteimg\" src=\""+file.url+"\" alt=\""+file.comment+"\" />";
				}
			}
		}
		} catch (e) {
			console.log(e);
		}
	}

	// is this a reply? if so...
	if (note.reply !== undefined && note.text !== null) {
		let replyname;

		// render replied to's username
		replyname = "@" + note.reply.user.host;
		try {
			replyname = replyname === "@null" ?
				"@" + note.reply.user.username :
				"@" + note.reply.user.username + replyname;
		} catch (e) {
			console.log(note.id);
			return;
		}
		let inlinepfp = `<img class="pfp inline" src="`+note.reply.user.avatarUrl+`" alt="`+replyname+`" />`;

		/* render replied-to note. overview of lines:
		 * 1: condition of both the reply and the replied-to have a CW
		 * 2: rendering for the above
		 * 3: only the replied-to has a CW
		 * 4: rendering for the above
		 * 5: only the reply has a CW
		 * 6: rendering for the above
		 * 7: rendering for no cw on any
		*/
		text = typeof (note.cw) == "string" && typeof (note.reply.cw) == "string" ?
			"<small><span class=\"rn\">" + inlinepfp + replyname + ` <a href="javascript:void(rendernote('` + note.reply.id + `', true))">said</a>:</span><br><details><summary>` + emojify(sanitize(note.reply.cw), note.reply.emojis) + `</summary><p>` + emojify(sanitize(note.reply.text), note.reply.emojis) + `</p></details></small><br><details><summary>` + emojify(sanitize(note.cw), note.emojis) + `</summary><p>` + emojify(sanitize(note.text), note.emojis) + `</p></details>` :
			typeof (note.reply.cw) == "string" ?
			"<small><span class=\"rn\">" + inlinepfp + replyname + ` <a href="javascript:void(rendernote('` + note.reply.id + `', true))">said</a>:</span><br><details><summary>` + emojify(sanitize(note.reply.cw), note.reply.emojis) + `</summary><p>` + emojify(sanitize(note.reply.text), note.reply.emojis) + `</p></details></small><br><br>` + emojify(sanitize(note.text), note.emojis) :
			typeof (note.cw) == "string" ?
			"<small><span class=\"rn\">" + inlinepfp + replyname + ` <a href="javascript:void(rendernote('` + note.reply.id + `', true))">said</a>:</span><br>` + emojify(sanitize(note.reply.text), note.reply.emojis) + `</small><br><br><details><summary>` + emojify(sanitize(note.cw), note.emojis) + `</summary><p>` + emojify(sanitize(note.text), note.emojis) + `</p></details>` :
			"<small><span class=\"rn\">" + inlinepfp + replyname + ` <a href="javascript:void(rendernote('` + note.reply.id + `', true))">said</a>:</span><br>` + emojify(sanitize(note.reply.text), note.reply.emojis) + `</small><br><br>` + emojify(sanitize(note.text), note.emojis);
	}

	// create the note container
	let notecont = document.createElement("div");
	notecont.id = note.id;
	notecont.className = "notecont";

	if (note.renote) {
		let rntext = document.createElement("p");
		rntext.className = "rn";
		try {
			rntext.insertAdjacentHTML("beforeend", renotetext);
			notecont.append(rntext);
		} catch (e) {
			return null;
		}
	}
	// profile section
	let profile = document.createElement("div");
	profile.className = "profile";

	// add profile picture
	let pfp = document.createElement("img");
	pfp.className = catclass;
	pfp.src = note.renote ? note.renote.user.avatarUrl : note.user.avatarUrl;
	pfp.alt = note.renote ? note.renote.user.username : note.user.username;
	profile.append(pfp);

	// handle the name and username rendering
	let userspan = document.createElement("span");
	let dispname = document.createElement("span");
	let handle = document.createElement("span");

	// display name
	dispname.insertAdjacentHTML("beforeend", name);
	dispname.className = "dispname";
	userspan.append(dispname);

	// username rendering
	handle.append(host);
	handle.className = "handle";
	userspan.append(handle);

	// add the profile section created above to the note container
	profile.append(userspan);
	notecont.append(profile);

	// create the actual note content, and import the values made previously
	let p = document.createElement("p");
	p.className = "notecontent";
	try {
		p.insertAdjacentHTML("beforeend", text);
		notecont.append(p);
	} catch (e) {
		return null;
	}

	// create reply and renote buttons
	let buttons = document.createElement("div");
	buttons.className = "buttons";
	let replybt = document.createElement("a");
	let renotte = document.createElement("a");

	// i'm aware these use JS links; I don't know of any better methods for now, but if you can implement it feel free to open a PR.
	replybt.href = "javascript:void(replier('" + noteid + "'))"
	renotte.href = "javascript:void(renote('" + noteid + "'))"

	// this is not an XSS vulnerability, as the content is static
	replybt.innerHTML = `<svg focusable="false" height="16px" viewBox="0 0 16 16" width="16px">
        <title>Reply</title>
        <path d="m 11.992188 3 v 4 c 0.003906 0.667969 -0.179688 0.726562 -0.441407 0.855469 c -0.265625 0.128906 -0.558593 0.144531 -0.558593 0.144531 h -5.589844 l 0.285156 -0.28125 c 0.183594 -0.191406 0.3125 -0.460938 0.3125 -0.71875 v -1 h -1 c -0.308594 0 -0.550781 0.089844 -0.75 0.28125 l -2.65625 2.699219 l 2.65625 2.738281 c 0.199219 0.191406 0.441406 0.28125 0.75 0.28125 h 1 v -1 c 0 -0.257812 -0.128906 -0.527344 -0.3125 -0.71875 l -0.277344 -0.28125 h 5.582032 s 0.710937 0.015625 1.453124 -0.355469 c 0.738282 -0.367187 1.550782 -1.3125 1.546876 -2.644531 v -4 z m 0 0" fill="var(--color)"/>
      </svg>`;
	renotte.innerHTML = `<svg focusable="false" height="16px" viewBox="0 0 16 16" width="16px">
        <title>Renote</title>
        <g fill="var(--color)">
          <path d="m 7.957031 2 c -0.082031 0 -0.164062 0.003906 -0.246093 0.007812 c -0.1875 0.011719 -0.375 0.03125 -0.5625 0.0625 c -1.582032 0.226563 -3.007813 1.070313 -3.96875 2.34375 c -0.804688 1.074219 -1.183594 2.332032 -1.179688 3.585938 h 2.003906 c 0 -0.832031 0.253906 -1.671875 0.796875 -2.398438 c 1.335938 -1.777343 3.820313 -2.113281 5.597657 -0.78125 c 0.429687 0.320313 0.769531 0.734376 1.03125 1.1875 h -1.4375 c -0.550782 0 -1 0.449219 -1 1 v 1 h 6 v -6 h -1 c -0.550782 0 -1 0.449219 -1 1 v 1.6875 c -1.113282 -1.695312 -3.007813 -2.710937 -5.039063 -2.695312 z m 0 0"/><path d="m 8.035156 15.007812 c 0.082032 0 0.164063 -0.003906 0.246094 -0.007812 c 0.1875 -0.011719 0.375 -0.03125 0.5625 -0.0625 c 1.582031 -0.226562 3.007812 -1.066406 3.96875 -2.34375 c 0.804688 -1.074219 1.183594 -2.332031 1.179688 -3.585938 h -2.003907 c -0.003906 0.832032 -0.257812 1.675782 -0.796875 2.398438 c -1.335937 1.777344 -3.820312 2.113281 -5.597656 0.78125 c -0.429688 -0.320312 -0.769531 -0.734375 -1.03125 -1.1875 h 1.4375 c 0.550781 0 1 -0.449219 1 -1 v -1 h -6 v 6 h 1 c 0.550781 0 1 -0.449219 1 -1 v -1.6875 c 1.113281 1.695312 3.007812 2.710938 5.035156 2.695312 z m 0 0"/>
        </g>
      </svg>`;

	if (note.visibility === "public" || note.visibility === "home") {
		buttons.append(replybt);
		buttons.append(renotte);
	} else {
		buttons.append(replybt);
		renotte = null; // discard renote button on followers and specified notes
	}

	// append the buttons
	notecont.append(buttons);

	// finally add the note container to the timeline
	var tl = document.getElementById("tl");
	tl.prepend(notecont);

	if (scroll) {
		document.getElementById(noteid).scrollIntoView();
	}
}

/* this function renders a note given a note id
 * using the above function.
 * by default, it will scroll to the created note.
 * if you want user input for this,
 * consider using notefetch() for that.
 *
 * Arguments:
 * 1. noteid - string of the note ID that you want to render;
*/

function rendernote(noteid) {
	var indat = getdat();

	xhr.open("POST", "https://" + indat[0] + "/api/notes/show");
	xhr.setRequestHeader("Accept", "application/json");
	xhr.setRequestHeader("Content-Type", "application/json");

	xhr.onreadystatechange = function () {
		if (xhr.readyState === 4) {
			let note = JSON.parse(xhr.response);
			innerhtmlmaker(note, true);
		}
	}

	xhr.send(JSON.stringify({
		"i": indat[1],
		"noteId": noteid
	}));
}

/* This function creates a note
 * with the visibility specified in the dropdown
 * in the buttons container.
 * It prompts for your note to enter.
 * Only use this function if you are ABSOLUTELY SURE that
 * the posted note does not have a CW.
 * If you want the user to decide, consider using noter().
*/

function note() {
	// get variables
	var notetext = window.prompt("Please enter your note");
	var indat = getdat();

	// request
	xhr.open("POST", "https://" + indat[0] + "/api/notes/create");
	xhr.setRequestHeader("Accept", "application/json");
	xhr.setRequestHeader("Content-Type", "application/json");

	let notedata = {
		"i": indat[1],
		"visibility": "public",
		"text": notetext,
		"visibility": document.getElementById("vis").value
	};

	xhr.onreadystatechange = function () {
		if (xhr.readyState === 4) {
			let note = JSON.parse(xhr.response);
			rendernote(note.createdNote.id);
		}
	}
	xhr.send(JSON.stringify(notedata));
}

/* This function sends a renote
 * with the visibility specified in the dropdown
 * in the buttons container.
 * It does not support quoting.
 *
 * Arguments:
 * 1. noteid - string of the note ID of the note being renoted
*/

function renote(noteid) {
	var indat = getdat();

	xhr.open("POST", "https://" + indat[0] + "/api/notes/create");
	xhr.setRequestHeader("Accept", "application/json");
	xhr.setRequestHeader("Content-Type", "application/json");

	let notedata = {
		"i": indat[1],
		"visibility": "public",
		"renoteId": noteid,
		"visibility": document.getElementById("vis").value
	};

	xhr.onreadystatechange = function () {
		if (xhr.readyState === 4) {
			let note = JSON.parse(xhr.response);
			rendernote(note.createdNote.id);
		}
	}
	xhr.send(JSON.stringify(notedata));
}

function refreshtl() {
	var indat = getdat();

	xhr.open("POST", "https://" + indat[0] + "/api/notes/hybrid-timeline");
	xhr.setRequestHeader("Accept", "application/json");
	xhr.setRequestHeader("Content-Type", "application/json");

	let tldata = {
		"i": indat[1],
		"limit": 100,
		"withFiles": false,
		"includeMyRenotes": true
	};

	tl.innerHTML = "";
	xhr.onreadystatechange = function () {
		if (xhr.readyState === 4) {
			let notes = JSON.parse(xhr.response);
			var tl = document.getElementById("tl");
			for (let i = 99; i > 0; i--) {
				innerhtmlmaker(notes[i], false);
			}
		}
	}
	xhr.send(JSON.stringify(tldata));
}

function mentfetch() {
	var indat = getdat();

	xhr.open("POST", "https://" + indat[0] + "/api/notes/mentions");
	xhr.setRequestHeader("Accept", "application/json");
	xhr.setRequestHeader("Content-Type", "application/json");

	let tldata = {
		"i": indat[1],
		"limit": 25
	};

	tl.innerHTML = "";
	xhr.onreadystatechange = function () {
		if (xhr.readyState === 4) {
			let notes = JSON.parse(xhr.response);
			var tl = document.getElementById("tl");
			for (let i = 24; i > 0; i--) {
				innerhtmlmaker(notes[i], false);
			}
		}
	}
	xhr.send(JSON.stringify(tldata));
}

// This function prompts for a note ID, then renders that note.

function notefetch() {
	rendernote(window.prompt("Enter the ID of the note you want to render."));
}

function reply(noteid) {
	let notetext = window.prompt("What do you want to reply with?");
	var indat = getdat();

	xhr.open("POST", "https://" + indat[0] + "/api/notes/create");
	xhr.setRequestHeader("Accept", "application/json");
	xhr.setRequestHeader("Content-Type", "application/json");

	let notedata = {
		"i": indat[1],
		"visibility": "public",
		"replyId": noteid,
		"text": notetext,
		"visibility": document.getElementById("vis").value
	};

	xhr.onreadystatechange = function () {
		if (xhr.readyState === 4) {
			let note = JSON.parse(xhr.response);
			rendernote(note.createdNote.id);
		}
	}
	xhr.send(JSON.stringify(notedata));
}

function cwnote() {
	// get variables
	var cw = window.prompt("Please enter your CW");
	var notetext = window.prompt("Please enter your note");
	var indat = getdat();

	// request
	xhr.open("POST", "https://" + indat[0] + "/api/notes/create");
	xhr.setRequestHeader("Accept", "application/json");
	xhr.setRequestHeader("Content-Type", "application/json");

	let notedata = {
		"i": indat[1],
		"visibility": "public",
		"cw": cw,
		"text": notetext,
		"visibility": document.getElementById("vis").value
	};

	xhr.onreadystatechange = function () {
		if (xhr.readyState === 4) {
			let note = JSON.parse(xhr.response);
			rendernote(note.createdNote.id);
		}
	}
	xhr.send(JSON.stringify(notedata));
}

function replycw(noteid) {
	let cw = window.prompt("Please enter your CW");
	let notetext = window.prompt("What do you want to reply with?");
	var indat = getdat();

	xhr.open("POST", "https://" + indat[0] + "/api/notes/create");
	xhr.setRequestHeader("Accept", "application/json");
	xhr.setRequestHeader("Content-Type", "application/json");

	let notedata = {
		"i": indat[1],
		"visibility": "public",
		"replyId": noteid,
		"cw": cw,
		"text": notetext,
		"visibility": document.getElementById("vis").value
	};

	xhr.onreadystatechange = function () {
		if (xhr.readyState === 4) {
			let note = JSON.parse(xhr.response);
			rendernote(note.createdNote.id);
		}
	}
	xhr.send(JSON.stringify(notedata));
}

function replier(noteid) {
	let cw = window.confirm("Do you want this note to not have a CW?");
	if (!cw) {
		replycw(noteid);
	} else {
		reply(noteid);
	}
}

function notecwchos() {
	let cw = window.confirm("Do you want this note to not have a CW?");
	if (!cw) {
		cwnote();
	} else {
		note();
	}
}
